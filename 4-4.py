# Опишіть свій клас винятку. Напишіть функцію, яка викидатиме цей виняток, якщо користувач введе певне значення,
# і перехопіть цей виняток під час виклику функції.

def check_age(age):
    if age < 18:
        raise ValueError("You must be at least 18 years old.")
    else:
        print("You are an adult.")


try:
    user_age = int(input("Enter your age: "))
    check_age(user_age)
except ValueError as e:
    print(e)

