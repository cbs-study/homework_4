# Створіть програму спортивного комплексу, у якій передбачене меню: 1 - перелік видів спорту,
# 2 - команда тренерів, 3 - розклад тренувань, 4 - вартість тренування. Дані зберігати у словниках.
# Також передбачити пошук по прізвищу тренера, яке вводиться з клавіатури у відповідному пункті меню.
# Якщо ключ не буде знайдений, створити відповідний клас-Exception, який буде викликатися в обробнику виключень

class CoachNotFoundException(Exception):
    pass


class SportsComplex:
    def __init__(self):
        self.sports = {
            '1': 'Football',
            '2': 'Basketball',
            '3': 'Volleyball'
        }
        self.coaches = {
            'Jack Wilson': 'Football',
            'Mark Moss': 'Basketball',
            'Jennifer Nielsen': 'Volleyball'
        }
        self.schedule = {
            'Monday': 'Football',
            'Tuesday': 'Basketball',
            'Thursday': 'Volleyball'
        }
        self.price = {
            'Football': '$70',
            'Basketball': '$65',
            'Volleyball': '$65'
        }

    def sports_list(self):
        print("The list of sports:")
        for sport in self.sports.items():
            print(f"{sport}")

    def coaches_list(self):
        print("The list of coaches: :")
        for coach, sport in self.coaches.items():
            print(f"{coach} - {sport}")

    def training_schedule(self):
        print("Training schedule:")
        for day, sport in self.schedule.items():
            print(f"{day}: {sport}")

    def training_price(self):
        print("Training prices:")
        for sport, price in self.price.items():
            print(f"{sport}: {price}")

    def find_coach(self, coach_name):
        if coach_name in self.coaches:
            print(f"The coach for {self.coaches[coach_name]} is {coach_name}.")
        else:
            raise CoachNotFoundException(f"The coach '{coach_name}' not found.")


def main():
    sports_complex = SportsComplex()
    while True:
        print("\n Menu:")
        print("1 - List of sports")
        print("2 - List of coaches")
        print("3 - Training schedule")
        print("4 - Training price")
        print("5 - Find a coach by name")
        print("6 - Exit")
        choice = input("Enter your choice: ")
        if choice == '1':
            sports_complex.sports_list()
        elif choice == '2':
            sports_complex.coaches_list()
        elif choice == '3':
            sports_complex.training_schedule()
        elif choice == '4':
            sports_complex.training_price()
        elif choice == '5':
            coach_name = input("Enter coach's name: ")
            try:
                sports_complex.find_coach(coach_name)
            except CoachNotFoundException as e:
                print(e)
        elif choice == '6':
            print("See you next time.")
            break
        else:
            print("Please enter a valid option.")


if __name__ == "__main__":
    main()

