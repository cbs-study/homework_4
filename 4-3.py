# Опишіть клас співробітника, який вміщує такі поля: ім'я, прізвище, відділ і рік початку роботи.
# Конструктор має генерувати виняток, якщо вказано неправильні дані. Введіть список працівників із клавіатури.
# Виведіть усіх співробітників, які були прийняті після цього року.

class Employee:
    def __init__(self, first_name, last_name, department, year_of_employment):
        self.first_name = first_name
        self.last_name = last_name
        self.department = department
        self.year_of_employment = year_of_employment


try:
    employees = []
    while True:
        first_name = input("Enter first name: ")
        last_name = input("Enter last name: ")
        department = input("Enter department: ")
        year_of_employment = int(input("Enter year of employment: "))

        employees.append(Employee(first_name, last_name, department, year_of_employment))

        add_employee = input("Do you want to add more employees? yes/no: ")
        if add_employee.lower() != 'yes':
            break

    filter_by_year = int(input("Enter the year to filter employees: "))

    print(f"Employees hired after {filter_by_year}:")
    for employee in employees:
        if employee.year_of_employment > filter_by_year:
            print(f"{employee.first_name} {employee.last_name} - {employee.department} - "
                  f"Year of employment: {employee.year_of_employment}")

except ValueError as ve:
    print("Error:", ve)
except Exception as e:
    print("Error occurred:", e)