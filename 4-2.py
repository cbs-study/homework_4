# Напишіть програму-калькулятор, яка підтримує такі операції: додавання, віднімання, множення, ділення та піднесення
# до ступеня. Програма має видавати повідомлення про помилку та продовжувати роботу під час введення некоректних даних,
# діленні на нуль та зведенні нуля в негативний степінь.


def addition(a, b):  # 1
    return a + b


def subtraction(a, b):  # 2
    return a - b


def multiplication(a, b):  # 3
    return a * b


def division(a, b):  # 4
    if b == 0:
        raise ValueError("Impossible to divide by zero")
    return a / b


def power(a, b):  # 5
    if a == 0 and b < 0:
        raise ValueError("Zero cannot be raised to a negative power")
    return a ** b


while True:
    try:
        print("Choose an operation:")
        print("1. Addition")
        print("2. Subtraction")
        print("3. Multiplication")
        print("4. Division")
        print("5. Power")
        print("6. Exit")

        operation = int(input("Enter the number of the operation: "))

        if operation == 6:
            break
        a = float(input("Enter the first number: "))
        b = float(input("Enter the second number: "))

        if operation == 1:
            result = addition(a, b)
        elif operation == 2:
            result = subtraction(a, b)
        elif operation == 3:
            result = multiplication(a, b)
        elif operation == 4:
            result = division(a, b)
        elif operation == 5:
            result = power(a, b)
        else:
            print("Invalid operation.")
            continue

        print("Result:", result)

    except ValueError as e:
        print("Error:", e)

    except Exception as e:
        print("An error occurred:", e)
